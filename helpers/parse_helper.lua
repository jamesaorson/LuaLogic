local Circuit = require "models.circuit"
local GateHelper = require "helpers.gate_helper"
local LogicStates = require "constants.logic_states"
local ParsingModes = require "constants.parsing_modes"
local Wire = require "gates.wire"

local M = {}

local find_wire
local parse_gate
local parse_gate_error_handler
local parse_wire
local parse_wire_error_handler
local tokenize_string

function find_wire(wireId, wires)
    return wires[wireId]
end

function parse_circuit_info(line, circuit)
    local tokens = tokenize_string(line)
    if #tokens ~= 1 then
        error("Incorrect number of fields for circuit info")
    end
    circuit.id = tokens[1]
end

function parse_circuit_info_error_handler(err)
    print("Error in parse_circuit_info()" .. err)
end

function parse_gate(line, circuit)
    local gates = circuit.gates
    local wires = circuit.wires

    local tokens = tokenize_string(line)
    if #tokens < 5 then
        error("Not enough fields for gate definition: " .. line)
    end
    local gateType = tokens[1]
    local delay = tonumber(tokens[2])
    local numberOfInputs = tonumber(tokens[3])
    if #tokens < numberOfInputs + 5 then
        error("Incorrect number of fields for number of inputs: " .. line)
    end
    local gate = GateHelper.create_gate(gateType, numberOfInputs, delay)
    for i = 4, 4 + numberOfInputs - 1 do
        local inputWireId = tokens[i]

        local inputWire = find_wire(inputWireId, wires)
        if inputWire == nil then
            inputWire = Wire(inputWireId, LogicStates.UNKNOWN)
            wires[inputWireId] = inputWire
        end
        GateHelper.add_input_wire(gate, inputWire)
    end
    local numberOfOutputs = tonumber(tokens[4 + numberOfInputs])
    for i = 4 + numberOfInputs + 1, 4 + numberOfInputs + numberOfOutputs do
        local outputWireId = tokens[i]

        local outputWire = find_wire(outputWireId, wires)
        if outputWire == nil then
            outputWire = Wire(outputWireId, LogicStates.UNKNOWN)
            wires[outputWireId] = outputWire
        end
        GateHelper.add_output_wire(gate, outputWire)
    end
    table.insert(gates, gate)
end

function parse_gate_error_handler(err)
    print("Error in parseGate(): " .. err)
end

function parse_wire(line, circuit)
    local gates = circuit.gates
    local wires = circuit.wires

    local tokens = tokenize_string(line)
    if #tokens ~= 2 then
        error("Not enough fields for wire definition: " .. line)
    end
    local wireId = tokens[1]
    local state = tokens[2]
    if string.lower(state) == "true" then
        state = LogicStates.TRUE
    elseif string.lower(state) == "false" then
        state = LogicStates.FALSE
    elseif string.lower(state) == "unknown" then
        state = LogicStates.UNKNOWN
    elseif tonumber(state) == LogicStates.TRUE then
        state = LogicStates.TRUE
    elseif tonumber(state) == LogicStates.FALSE then
        state = LogicStates.FALSE
    elseif tonumber(state) == LogicStates.UNKNOWN then
        state = LogicStates.UNKNOWN
    else
        error("Invalid logic state: " .. line)
    end

    local wire = find_wire(wireId, wires)
    if wire == nil then
        error("Wire with id " .. wireId .. " does not exist")
    end
    wire.state = state
end

function parse_wire_error_handler(err)
    print("Error in parseGate(): " .. err)
end

function tokenize_string(str)
    local tokens = {}
    for token in string.gmatch(str, "%S+") do
        table.insert(tokens, token)
    end
    return tokens
end

function M.parse_circuit(circuitFileName, outputFileName)
    local circuit = Circuit()
    local gates = circuit.gates
    local wires = circuit.wires

    local fileLines = io.lines(circuitFileName)
    local parsingMode = ParsingModes.UNKNOWN
    for line in fileLines do
        if string.sub(line, 1, 1) ~= "#" then
            if parsingMode == ParsingModes.UNKNOWN then
                parsingMode = ParsingModes.CIRCUIT_INFO
            end
            if parsingMode == ParsingModes.CIRCUIT_INFO then
                if line == "[Gates]" then
                    parsingMode = ParsingModes.GATES
                else
                    xpcall(parse_circuit_info, parse_circuit_info_error_handler, line, circuit)
                end
            elseif parsingMode == ParsingModes.GATES then
                if line == "[Wires]" then
                    parsingMode = ParsingModes.WIRES
                else
                    xpcall(parse_gate, parse_gate_error_handler, line, circuit)
                end
            elseif parsingMode == ParsingModes.WIRES then
                xpcall(parse_wire, parse_wire_error_handler, line, circuit)
            else
                error("Circuit definition file was malformed at: " .. line)
            end
        end
    end

    return circuit
end

return M
