local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"

local And = require "gates.and"
local Nand = require "gates.nand"
local Nor = require "gates.nor"
local Not = require "gates.not"
local Or = require "gates.or"
local Xnor = require "gates.xnor"
local Xor = require "gates.xor"

local M = {}

function M.add_input_wire(gate, inputWire)
    if #gate.inputs < gate.numberOfInputs then
        table.insert(gate.inputs, inputWire)
        if inputWire.outputGate ~= nil then
            for i, wire in ipairs(inputWire.outputGate.inputs) do
                if wire == inputWire then
                    table.remove(inputWire.outputGate.inputs, i)
                    break
                end
            end
        end
        inputWire.outputGate = gate
    end
end

function M.add_output_wire(gate, outputWire)
    table.insert(gate.outputs, outputWire)
    if outputWire.inputGate ~= nil then
        for i, _ in ipairs(outputWire.inputGate.outputs) do
            table.remove(outputWire.inputGate.outputs, i)
        end
    end
    outputWire.inputGate = gate
end

function M.create_gate(type, numberOfInputs, delay)
    if type == GateInfo.AND.displayName      or type == GateInfo.AND.type then
        return And(numberOfInputs, delay)
    elseif type == GateInfo.OR.displayName   or type == GateInfo.OR.type then
        return Or(numberOfInputs, delay)
    elseif type == GateInfo.NOT.displayName  or type == GateInfo.NOT.type then
        return Not(delay)
    elseif type == GateInfo.XOR.displayName  or type == GateInfo.XOR.type then
        return Xor(numberOfInputs, delay)
    elseif type == GateInfo.NAND.displayName or type == GateInfo.NAND.type then
        return Nand(numberOfInputs, delay)
    elseif type == GateInfo.NOR.displayName  or type == GateInfo.NOR.type then
        return Nor(numberOfInputs, delay)
    elseif type == GateInfo.XNOR.displayName or type == GateInfo.XNOR.type then
        return Xnor(numberOfInputs, delay)
    else
        return nil
    end
end

function M.get_display_name_from_type(gateType)
    for gate, info in pairs(GateInfo) do
        if info.type == gateType then
            return info.displayName
        end
    end 
    return nil
end

function M.print_gate(gate, outputFileName)
    local resultString = ""
    resultString = resultString .. "Type: " .. M.get_display_name_from_type(gate.type) .. "\n"
    resultString = resultString .. "Delay: " .. gate.delay .. "\n"
    resultString = resultString .. "Number of inputs: " .. gate.numberOfInputs .. "\n"
    if gate.inputs ~= nil then
        for i, inputWire in ipairs(gate.inputs) do
            resultString = resultString .. "Input " .. inputWire.id .. ": " .. inputWire.history .. "\n"
        end
    end
    
    for _, outputWire in ipairs(gate.outputs) do
        resultString = resultString .. "Output " .. outputWire.id .. ": " .. outputWire.history .. "\n"
    end

    if outputFileName == nil then
        print(resultString)
    else
        local outputFile = assert(io.open(outputFileName, "a"))
        outputFile:write(resultString)
        outputFile:close()
    end
end

return M