local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local Xor = require "gates.xor"

local maxNumberOfInputs = 10

describe(
    "Xor gate -",
    function()
        describe(
            "Functions - ",
            function()
                it(
                    "Xor() should call initalize()",
                    function()
                        spy.on(Xor, "initialize")
                        local xorGate = Xor(2, 1)

                        assert.spy(Xor.initialize).was.called()
                    end
                )
                it(
                    "initalize() should set fields",
                    function()
                        local numberOfInputs = 2
                        local delay = 1
                        local xorGate = Xor(numberOfInputs, delay)
                        
                        assert.equals(#xorGate.inputs, 0)
                        assert.equals(#xorGate.outputs, 0)
                        assert.equals(xorGate.delay, delay)
                        assert.equals(xorGate.numberOfInputs, numberOfInputs)
                        assert.equals(xorGate.type, GateInfo.XOR.type)
                    end
                )
                it(
                    "do_logic() should call simulate_logic()",
                    function()
                        local xorGate = Xor(2, 1)
                        GateHelper.add_input_wire(xorGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_input_wire(xorGate, Wire(2, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(xorGate, Wire(3, LogicStates.UNKNOWN))
                        spy.on(xorGate, "simulate_logic")
                        xorGate:do_logic()
                
                        assert.spy(xorGate.simulate_logic).was.called()
                    end
                )
            end
        )
        describe(
            "logic -",
            function()
                it(
                    "should have true outputs if an odd number of inputs are true and no inputs are unknown",
                    function()
                        for i = 3, maxNumberOfInputs, 2 do
                            local xorGate = Xor(i, 1)
                            for j = 1, i do
                                local state = LogicStates.TRUE
                                if i % 2 == 0 then
                                    state = LogicStates.FALSE
                                end
                                GateHelper.add_input_wire(xorGate, Wire(j, state))
                            end
                            GateHelper.add_output_wire(xorGate, Wire(i + 1, LogicStates.UNKNOWN))

                            xorGate:do_logic()
                            assert.is_true(xorGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have false outputs if an even number of inputs are true and no inputs are unknown",
                    function()
                        for i = 2, maxNumberOfInputs, 2 do
                            local xorGate = Xor(i, 1)
                            for j = 1, i do
                                local state = LogicStates.TRUE
                                if i % 2 == 0 then
                                    state = LogicStates.FALSE
                                end
                                GateHelper.add_input_wire(xorGate, Wire(j, state))
                            end
                            GateHelper.add_output_wire(xorGate, Wire(i + 1, LogicStates.UNKNOWN))

                            xorGate:do_logic()
                            assert.is_true(xorGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have unknown outputs if any input is unknown",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local xorGate = Xor(i, 1)
                            for j = 1, i - 1 do
                                local state = LogicStates.TRUE
                                if i % 2 == 0 then
                                    state = LogicStates.FALSE
                                end
                                GateHelper.add_input_wire(xorGate, Wire(j, state))
                            end
                            GateHelper.add_input_wire(xorGate, Wire(i, LogicStates.UNKNOWN))
                            GateHelper.add_output_wire(xorGate, Wire(i + 1, LogicStates.UNKNOWN))

                            xorGate:do_logic()
                            assert.is_true(xorGate.outputs[1].state == LogicStates.UNKNOWN)
                        end
                    end
                )
            end
        )
    end
)
