local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local Not = require "gates.not"

describe(
    "Not gate -",
    function()
        describe(
            "Functions - ",
            function()
                it(
                    "Not() should call initalize()",
                    function()
                        spy.on(Not, "initialize")
                        local notGate = Not(1)

                        assert.spy(Not.initialize).was.called()
                    end
                )
                it(
                    "initalize() should set fields",
                    function()
                        local delay = 1
                        local notGate = Not(delay)
                        
                        assert.equals(#notGate.inputs, 0)
                        assert.equals(#notGate.outputs, 0)
                        assert.equals(notGate.delay, delay)
                        assert.equals(notGate.numberOfInputs, 1)
                        assert.equals(notGate.type, GateInfo.NOT.type)
                    end
                )
                it(
                    "do_logic() should call simulate_logic()",
                    function()
                        local notGate = Not(1)
                        GateHelper.add_input_wire(notGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(notGate, Wire(3, LogicStates.UNKNOWN))
                        spy.on(notGate, "simulate_logic")
                        notGate:do_logic()
                
                        assert.spy(notGate.simulate_logic).was.called()
                    end
                )
            end
        )
        describe(
            "logic -",
            function()
                it(
                    "should have false outputs if input is true",
                    function()
                        local notGate = Not(1)
                        GateHelper.add_input_wire(notGate, Wire(1, LogicStates.TRUE))
                        GateHelper.add_output_wire(notGate, Wire(2, LogicStates.UNKNOWN))

                        notGate:do_logic()
                        assert.is_true(notGate.outputs[1].state == LogicStates.FALSE)
                    end
                )
                it(
                    "should have false outputs if input is true",
                    function()
                        local notGate = Not(1)
                        GateHelper.add_input_wire(notGate, Wire(1, LogicStates.FALSE))
                        GateHelper.add_output_wire(notGate, Wire(2, LogicStates.UNKNOWN))

                        notGate:do_logic()
                        assert.is_true(notGate.outputs[1].state == LogicStates.TRUE)
                    end
                )
                it(
                    "should have unnkown outputs if input is unknown",
                    function()
                        local notGate = Not(1)
                        GateHelper.add_input_wire(notGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(notGate, Wire(2, LogicStates.UNKNOWN))

                        notGate:do_logic()
                        assert.is_true(notGate.outputs[1].state == LogicStates.UNKNOWN)
                    end
                )
            end
        )
    end
)
