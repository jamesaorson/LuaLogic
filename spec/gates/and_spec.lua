local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local And = require "gates.and"

local maxNumberOfInputs = 10

describe(
    "And gate -",
    function()
        describe(
            "Functions - ",
            function()
                it(
                    "And() should call initalize()",
                    function()
                        spy.on(And, "initialize")
                        local andGate = And(2, 1)

                        assert.spy(And.initialize).was.called()
                    end
                )
                it(
                    "initalize() should set fields",
                    function()
                        local numberOfInputs = 2
                        local delay = 1
                        local andGate = And(numberOfInputs, delay)

                        assert.equals(#andGate.inputs, 0)
                        assert.equals(#andGate.outputs, 0)
                        assert.equals(andGate.delay, delay)
                        assert.equals(andGate.numberOfInputs, numberOfInputs)
                        assert.equals(andGate.type, GateInfo.AND.type)
                    end
                )
                it(
                    "do_logic() should call simulate_logic()",
                    function()
                        local andGate = And(2, 1)
                        GateHelper.add_input_wire(andGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_input_wire(andGate, Wire(2, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(andGate, Wire(3, LogicStates.UNKNOWN))
                        spy.on(andGate, "simulate_logic")
                        andGate:do_logic()
                
                        assert.spy(andGate.simulate_logic).was.called()
                    end
                )
            end
        )
        describe(
            "logic -",
            function()
                it(
                    "should have false outputs if any input is false",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local andGate = And(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(andGate, Wire(j, LogicStates.TRUE))
                            end
                            GateHelper.add_input_wire(andGate, Wire(i, LogicStates.FALSE))
                            GateHelper.add_output_wire(andGate, Wire(i + 1, LogicStates.UNKNOWN))

                            andGate:do_logic()
                            assert.is_true(andGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have true outputs if all inputs are true",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local andGate = And(i, 1)
                            for j = 1, i do
                                GateHelper.add_input_wire(andGate, Wire(j, LogicStates.TRUE))
                            end
                            GateHelper.add_output_wire(andGate, Wire(i + 1, LogicStates.UNKNOWN))

                            andGate:do_logic()
                            assert.is_true(andGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have false outputs if any input is false, even if other inputs are unknown",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local andGate = And(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(andGate, Wire(j, LogicStates.UNKNOWN))
                            end
                            GateHelper.add_input_wire(andGate, Wire(i, LogicStates.FALSE))
                            GateHelper.add_output_wire(andGate, Wire(i + 1, LogicStates.UNKNOWN))

                            andGate:do_logic()
                            assert.is_true(andGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have an unknown output if any input is unknown and all other inputs are true",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local andGate = And(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(andGate, Wire(j, LogicStates.TRUE))
                            end
                            GateHelper.add_input_wire(andGate, Wire(i, LogicStates.UNKNOWN))
                            GateHelper.add_output_wire(andGate, Wire(i + 1, LogicStates.UNKNOWN))

                            andGate:do_logic()
                            assert.is_true(andGate.outputs[1].state == LogicStates.UNKNOWN)
                        end
                    end
                )
            end
        )
    end
)
