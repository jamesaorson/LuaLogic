local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local Or = require "gates.or"

local maxNumberOfInputs = 10

describe(
    "Or gate -",
    function()
        describe(
            "Functions - ",
            function()
                it(
                    "Or() should call initalize()",
                    function()
                        spy.on(Or, "initialize")
                        local orGate = Or(2, 1)

                        assert.spy(Or.initialize).was.called()
                    end
                )
                it(
                    "initalize() should set fields",
                    function()
                        local numberOfInputs = 2
                        local delay = 1
                        local orGate = Or(numberOfInputs, delay)
                        
                        assert.equals(#orGate.inputs, 0)
                        assert.equals(#orGate.outputs, 0)
                        assert.equals(orGate.delay, delay)
                        assert.equals(orGate.numberOfInputs, numberOfInputs)
                        assert.equals(orGate.type, GateInfo.OR.type)
                    end
                )
                it(
                    "do_logic() should call simulate_logic()",
                    function()
                        local orGate = Or(2, 1)
                        GateHelper.add_input_wire(orGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_input_wire(orGate, Wire(2, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(orGate, Wire(3, LogicStates.UNKNOWN))
                        spy.on(orGate, "simulate_logic")
                        orGate:do_logic()
                
                        assert.spy(orGate.simulate_logic).was.called()
                    end
                )
            end
        )
        describe(
            "logic -",
            function()
                it(
                    "should have true outputs if any input is true",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local orGate = Or(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(orGate, Wire(j, LogicStates.FALSE))
                            end
                            GateHelper.add_input_wire(orGate, Wire(i, LogicStates.TRUE))
                            GateHelper.add_output_wire(orGate, Wire(i + 1, LogicStates.UNKNOWN))

                            orGate:do_logic()
                            assert.is_true(orGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have false outputs if all inputs are false",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local orGate = Or(i, 1)
                            for j = 1, i do
                                GateHelper.add_input_wire(orGate, Wire(j, LogicStates.FALSE))
                            end
                            GateHelper.add_output_wire(orGate, Wire(i + 1, LogicStates.UNKNOWN))

                            orGate:do_logic()
                            assert.is_true(orGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have true outputs if any input is true, even if other inputs are unknown",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local orGate = Or(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(orGate, Wire(j, LogicStates.UNKNOWN))
                            end
                            GateHelper.add_input_wire(orGate, Wire(i, LogicStates.TRUE))
                            GateHelper.add_output_wire(orGate, Wire(i + 1, LogicStates.UNKNOWN))

                            orGate:do_logic()
                            assert.is_true(orGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have unknown outputs if any input is unknown and all other inputs are false",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local orGate = Or(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(orGate, Wire(j, LogicStates.FALSE))
                            end
                            GateHelper.add_input_wire(orGate, Wire(i, LogicStates.UNKNOWN))
                            GateHelper.add_output_wire(orGate, Wire(i + 1, LogicStates.UNKNOWN))

                            orGate:do_logic()
                            assert.is_true(orGate.outputs[1].state == LogicStates.UNKNOWN)
                        end
                    end
                )
            end
        )
    end
)
