local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local Xnor = require "gates.xnor"

local maxNumberOfInputs = 10

describe(
    "Xnor gate -",
    function()
        describe(
            "Functions - ",
            function()
                it(
                    "Xnor() should call initalize()",
                    function()
                        spy.on(Xnor, "initialize")
                        local xnorGate = Xnor(2, 1)

                        assert.spy(Xnor.initialize).was.called()
                    end
                )
                it(
                    "initalize() should set fields",
                    function()
                        local numberOfInputs = 2
                        local delay = 1
                        local xnorGate = Xnor(numberOfInputs, delay)
                        
                        assert.equals(#xnorGate.inputs, 0)
                        assert.equals(#xnorGate.outputs, 0)
                        assert.equals(xnorGate.delay, delay)
                        assert.equals(xnorGate.numberOfInputs, numberOfInputs)
                        assert.equals(xnorGate.type, GateInfo.XNOR.type)
                    end
                )
                it(
                    "do_logic() should call simulate_logic()",
                    function()
                        local xnorGate = Xnor(2, 1)
                        GateHelper.add_input_wire(xnorGate, Wire(1, LogicStates.UNKNOWN))
                        GateHelper.add_input_wire(xnorGate, Wire(2, LogicStates.UNKNOWN))
                        GateHelper.add_output_wire(xnorGate, Wire(3, LogicStates.UNKNOWN))
                        spy.on(xnorGate, "simulate_logic")
                        xnorGate:do_logic()
                
                        assert.spy(xnorGate.simulate_logic).was.called()
                    end
                )
            end
        )
        describe(
            "logic -",
            function()
                it(
                    "should have false outputs if an odd number of inputs are true and no inputs are unknown",
                    function()
                        for i = 3, maxNumberOfInputs, 2 do
                            local xnorGate = Xnor(i, 1)
                            for j = 1, i do
                                local state = LogicStates.TRUE
                                if i % 2 == 0 then
                                    state = LogicStates.FALSE
                                end
                                GateHelper.add_input_wire(xnorGate, Wire(j, state))
                            end
                            GateHelper.add_output_wire(xnorGate, Wire(i + 1, LogicStates.UNKNOWN))

                            xnorGate:do_logic()
                            assert.is_true(xnorGate.outputs[1].state == LogicStates.FALSE)
                        end
                    end
                )
                it(
                    "should have true outputs if an even number of inputs are true and no inputs are unknown",
                    function()
                        for i = 2, maxNumberOfInputs, 2 do
                            local xnorGate = Xnor(i, 1)
                            for j = 1, i do
                                local state = LogicStates.TRUE
                                if i % 2 == 0 then
                                    state = LogicStates.FALSE
                                end
                                GateHelper.add_input_wire(xnorGate, Wire(j, state))
                            end
                            GateHelper.add_output_wire(xnorGate, Wire(i + 1, LogicStates.UNKNOWN))

                            xnorGate:do_logic()
                            assert.is_true(xnorGate.outputs[1].state == LogicStates.TRUE)
                        end
                    end
                )
                it(
                    "should have unknown outputs if any input is unknown",
                    function()
                        for i = 2, maxNumberOfInputs do
                            local xnorGate = Xnor(i, 1)
                            for j = 1, i - 1 do
                                GateHelper.add_input_wire(xnorGate, Wire(j, LogicStates.TRUE))
                            end
                            GateHelper.add_input_wire(xnorGate, Wire(i, LogicStates.UNKNOWN))
                            GateHelper.add_output_wire(xnorGate, Wire(i + 1, LogicStates.UNKNOWN))

                            xnorGate:do_logic()
                            assert.is_true(xnorGate.outputs[1].state == LogicStates.UNKNOWN)
                        end
                    end
                )
            end
        )
    end
)