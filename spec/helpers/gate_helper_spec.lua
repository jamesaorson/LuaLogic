local GateHelper = require "helpers.gate_helper"
local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"
local Wire = require "gates.wire"

local And = require "gates.and"
local Nand = require "gates.nand"
local Nor = require "gates.nor"
local Not = require "gates.not"
local Or = require "gates.or"
local Xnor = require "gates.xnor"
local Xor = require "gates.xor"

local delay = 1

describe(
    "Gate helper -",
    function()
        describe(
            "add_input_wire - ",
            function()
                describe(
                    "And gate - ",
                    function()
                        local andGate
                        local numberOfInputs = 2
                        before_each(function()
                            andGate = GateHelper.create_gate(GateInfo.AND.type, numberOfInputs, delay)
                        end)

                        it(
                            "should add a wire if And gate is not filled",
                            function()
                                assert.equals(#andGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(andGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#andGate.inputs, i)
                                end
                            end
                        )
                        it(
                            "should not add a wire if And gate is filled",
                            function()
                                assert.equals(#andGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(andGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#andGate.inputs, i)
                                end
                                GateHelper.add_input_wire(andGate, Wire(numberOfInputs + 1, LogicStates.UNKNOWN))
                                assert.equals(#andGate.inputs, numberOfInputs)
                            end
                        )
                    end
                )
                describe(
                    "Nand gate - ",
                    function()
                        local nandGate
                        local numberOfInputs = 2
                        before_each(function()
                            nandGate = GateHelper.create_gate(GateInfo.NAND.type, numberOfInputs, delay)
                        end)

                        it(
                            "should add a wire if Nand gate is not filled",
                            function()
                                assert.equals(#nandGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(nandGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#nandGate.inputs, i)
                                end
                            end
                        )
                        it(
                            "should not add a wire if Nand gate is filled",
                            function()
                                assert.equals(#nandGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(nandGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#nandGate.inputs, i)
                                end
                                GateHelper.add_input_wire(nandGate, Wire(numberOfInputs + 1, LogicStates.UNKNOWN))
                                assert.equals(#nandGate.inputs, numberOfInputs)
                            end
                        )
                    end
                )
                describe(
                    "Nor gate - ",
                    function()
                        local norGate
                        local numberOfInputs = 2
                        before_each(function()
                            norGate = GateHelper.create_gate(GateInfo.NOR.type, numberOfInputs, delay)
                        end)

                        it(
                            "should add a wire if Nor gate is not filled",
                            function()
                                assert.equals(#norGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(norGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#norGate.inputs, i)
                                end
                            end
                        )
                        it(
                            "should not add a wire if Nor gate is filled",
                            function()
                                assert.equals(#norGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(norGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#norGate.inputs, i)
                                end
                                GateHelper.add_input_wire(norGate, Wire(numberOfInputs + 1, LogicStates.UNKNOWN))
                                assert.equals(#norGate.inputs, numberOfInputs)
                            end
                        )
                    end
                )
                describe(
                    "Not gate - ",
                    function()
                        local notGate
                        before_each(function()
                            notGate = GateHelper.create_gate(GateInfo.NOT.type, 1, delay)
                        end)

                        it(
                            "should add a wire if Not gate is not filled",
                            function()
                                assert.equals(#notGate.inputs, 0)
                                GateHelper.add_input_wire(notGate, Wire(1, LogicStates.UNKNOWN))
                                assert.equals(#notGate.inputs, 1)
                            end
                        )
                        it(
                            "should not add a wire if Not gate is filled",
                            function()
                                assert.equals(#notGate.inputs, 0)
                                GateHelper.add_input_wire(notGate, Wire(1, LogicStates.UNKNOWN))
                                assert.equals(#notGate.inputs, 1)
                                GateHelper.add_input_wire(notGate, Wire(2, LogicStates.UNKNOWN))
                                assert.equals(#notGate.inputs, 1)
                            end
                        )
                    end
                )
                describe(
                    "Or gate - ",
                    function()
                        local orGate
                        local numberOfInputs = 2
                        before_each(function()
                            orGate = GateHelper.create_gate(GateInfo.OR.type, numberOfInputs, delay)
                        end)

                        it(
                            "should add a wire if Or gate is not filled",
                            function()
                                assert.equals(#orGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(orGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#orGate.inputs, i)
                                end
                            end
                        )
                        it(
                            "should not add a wire if Or gate is filled",
                            function()
                                assert.equals(#orGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(orGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#orGate.inputs, i)
                                end
                                GateHelper.add_input_wire(orGate, Wire(numberOfInputs + 1, LogicStates.UNKNOWN))
                                assert.equals(#orGate.inputs, numberOfInputs)
                            end
                        )
                    end
                )
                describe(
                    "Xnor gate - ",
                    function()
                        local xnorGate
                        local numberOfInputs = 2
                        before_each(function()
                            xnorGate = GateHelper.create_gate(GateInfo.XNOR.type, numberOfInputs, delay)
                        end)

                        it(
                            "should add a wire if Xnor gate is not filled",
                            function()
                                assert.equals(#xnorGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(xnorGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#xnorGate.inputs, i)
                                end
                            end
                        )
                        it(
                            "should not add a wire if Xnor gate is filled",
                            function()
                                assert.equals(#xnorGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(xnorGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#xnorGate.inputs, i)
                                end
                                GateHelper.add_input_wire(xnorGate, Wire(numberOfInputs + 1, LogicStates.UNKNOWN))
                                assert.equals(#xnorGate.inputs, numberOfInputs)
                            end
                        )
                    end
                )
                describe(
                    "Xor gate - ",
                    function()
                        local xorGate
                        local numberOfInputs = 2
                        before_each(function()
                            xorGate = GateHelper.create_gate(GateInfo.XOR.type, numberOfInputs, delay)
                        end)

                        it(
                            "should add a wire if Xor gate is not filled",
                            function()
                                assert.equals(#xorGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(xorGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#xorGate.inputs, i)
                                end
                            end
                        )
                        it(
                            "should not add a wire if Xor gate is filled",
                            function()
                                assert.equals(#xorGate.inputs, 0)
                                for i = 1, numberOfInputs do
                                    GateHelper.add_input_wire(xorGate, Wire(i, LogicStates.UNKNOWN))
                                    assert.equals(#xorGate.inputs, i)
                                end
                                GateHelper.add_input_wire(xorGate, Wire(numberOfInputs + 1, LogicStates.UNKNOWN))
                                assert.equals(#xorGate.inputs, numberOfInputs)
                            end
                        )
                    end
                )
            end
        )
        describe(
            "create_gate - ",
            function()
                describe(
                    "And gate - ",
                    function()
                        local numberOfInputs = 2
                        it(
                            "should call And.initialize() when passed an And gate type",
                            function()
                                spy.on(And, "initialize")

                                local andGate = GateHelper.create_gate(GateInfo.AND.type, numberOfInputs, delay)

                                assert.spy(And.initialize).was.called()
                            end
                        )
                        it(
                            "should create And gate with correct fields when passed an And gate type",
                            function()
                                local andGate = GateHelper.create_gate(GateInfo.AND.type, numberOfInputs, delay)
                                assert.equals(#andGate.inputs, 0)
                                assert.equals(andGate.delay, delay)
                                assert.equals(andGate.numberOfInputs, numberOfInputs)
                                assert.equals(andGate.output, nil)
                                assert.equals(andGate.type, GateInfo.AND.type)
                            end
                        )
                    end
                )
                describe(
                    "Nand gate - ",
                    function()
                        local numberOfInputs = 2
                        it(
                            "should call Nand.initialize() when passed a Nand gate type",
                            function()
                                spy.on(Nand, "initialize")

                                local nandGate = GateHelper.create_gate(GateInfo.NAND.type, numberOfInputs, delay)

                                assert.spy(Nand.initialize).was.called()
                            end
                        )
                        it(
                            "should create And gate with correct fields when passed a Nand gate type",
                            function()
                                local nandGate = GateHelper.create_gate(GateInfo.NAND.type, numberOfInputs, delay)
                                assert.equals(#nandGate.inputs, 0)
                                assert.equals(nandGate.delay, delay)
                                assert.equals(nandGate.numberOfInputs, numberOfInputs)
                                assert.equals(nandGate.output, nil)
                                assert.equals(nandGate.type, GateInfo.NAND.type)
                            end
                        )
                    end
                )
                describe(
                    "Nor gate - ",
                    function()
                        local numberOfInputs = 2
                        it(
                            "should call Nor.initialize() when passed a Nor gate type",
                            function()
                                spy.on(Nor, "initialize")

                                local norGate = GateHelper.create_gate(GateInfo.NOR.type, numberOfInputs, delay)

                                assert.spy(Nor.initialize).was.called()
                            end
                        )
                        it(
                            "should create Nor gate with correct fields when passed a Nor gate type",
                            function()
                                local norGate = GateHelper.create_gate(GateInfo.NOR.type, numberOfInputs, delay)
                                assert.equals(#norGate.inputs, 0)
                                assert.equals(norGate.delay, delay)
                                assert.equals(norGate.numberOfInputs, numberOfInputs)
                                assert.equals(norGate.output, nil)
                                assert.equals(norGate.type, GateInfo.NOR.type)
                            end
                        )
                    end
                )
                describe(
                    "Not gate - ",
                    function()
                        it(
                            "should call Not.initialize() when passed a Not gate type",
                            function()
                                spy.on(Not, "initialize")

                                local notGate = GateHelper.create_gate(GateInfo.NOT.type, numberOfInputs, delay)

                                assert.spy(Not.initialize).was.called()
                            end
                        )
                        it(
                            "should create Not gate with correct fields when passed a Not gate type",
                            function()
                                local notGate = GateHelper.create_gate(GateInfo.NOT.type, 1, delay)
                                assert.equals(#notGate.inputs, 0)
                                assert.equals(notGate.delay, delay)
                                assert.equals(notGate.numberOfInputs, 1)
                                assert.equals(notGate.output, nil)
                                assert.equals(notGate.type, GateInfo.NOT.type)
                            end
                        )
                        it(
                            "should always set Not gate to have one input",
                            function()
                                local notGate = GateHelper.create_gate(GateInfo.NOT.type, 2, delay)
                                assert.equals(notGate.numberOfInputs, 1)
                            end
                        )
                    end
                )
                describe(
                    "Or gate - ",
                    function()
                        local numberOfInputs = 2
                        it(
                            "should call Or.initialize() when passed an Or gate type",
                            function()
                                spy.on(Or, "initialize")

                                local orGate = GateHelper.create_gate(GateInfo.OR.type, numberOfInputs, delay)

                                assert.spy(Or.initialize).was.called()
                            end
                        )
                        it(
                            "should create Or gate with correct fields when passed an Or gate type",
                            function()
                                local orGate = GateHelper.create_gate(GateInfo.OR.type, numberOfInputs, delay)
                                assert.equals(#orGate.inputs, 0)
                                assert.equals(orGate.delay, delay)
                                assert.equals(orGate.numberOfInputs, numberOfInputs)
                                assert.equals(orGate.output, nil)
                                assert.equals(orGate.type, GateInfo.OR.type)
                            end
                        )
                    end
                )
                describe(
                    "Xnor gate - ",
                    function()
                        local numberOfInputs = 2
                        it(
                            "should call Xnor.initialize() when passed an Xnor gate type",
                            function()
                                spy.on(Xnor, "initialize")

                                local xnorGate = GateHelper.create_gate(GateInfo.XNOR.type, numberOfInputs, delay)

                                assert.spy(Xnor.initialize).was.called()
                            end
                        )
                        it(
                            "should create Xnor gate with correct fields when passed an Xnor gate type",
                            function()
                                local xnorGate = GateHelper.create_gate(GateInfo.XNOR.type, numberOfInputs, delay)
                                assert.equals(#xnorGate.inputs, 0)
                                assert.equals(xnorGate.delay, delay)
                                assert.equals(xnorGate.numberOfInputs, numberOfInputs)
                                assert.equals(xnorGate.output, nil)
                                assert.equals(xnorGate.type, GateInfo.XNOR.type)
                            end
                        )
                    end
                )
                describe(
                    "Xor gate - ",
                    function()
                        local numberOfInputs = 2
                        it(
                            "should call Xor.initialize() when passed an Xor gate type",
                            function()
                                spy.on(Xor, "initialize")

                                local xorGate = GateHelper.create_gate(GateInfo.XOR.type, numberOfInputs, delay)

                                assert.spy(Xor.initialize).was.called()
                            end
                        )
                        it(
                            "should create Xor gate with correct fields when passed an Xor gate type",
                            function()
                                local xorGate = GateHelper.create_gate(GateInfo.XOR.type, numberOfInputs, delay)
                                assert.equals(#xorGate.inputs, 0)
                                assert.equals(xorGate.delay, delay)
                                assert.equals(xorGate.numberOfInputs, numberOfInputs)
                                assert.equals(xorGate.output, nil)
                                assert.equals(xorGate.type, GateInfo.XOR.type)
                            end
                        )
                    end
                )
                describe(
                    "Fake gate - ",
                    function()
                        local numberOfInputs = 2
                        it(
                            "should return nil when passed an improper gate type",
                            function()
                                local fakeGate = GateHelper.create_gate("", numberOfInputs, delay)
                                assert.is_nil(fakeGate)
                            end
                        )
                    end
                )
            end
        )
        describe(
            "get_display_name_from_type - ",
            function()
                it(
                    "should return And display name when passed And type",
                    function()
                        assert.equals(GateInfo.AND.displayName, GateHelper.get_display_name_from_type(GateInfo.AND.type))
                    end
                )
                it(
                    "should return Nand display name when passed Nand type",
                    function()
                        assert.equals(GateInfo.NAND.displayName, GateHelper.get_display_name_from_type(GateInfo.NAND.type))
                    end
                )
                it(
                    "should return Nor display name when passed Nor type",
                    function()
                        assert.equals(GateInfo.NOR.displayName, GateHelper.get_display_name_from_type(GateInfo.NOR.type))
                    end
                )
                it(
                    "should return Not display name when passed Not type",
                    function()
                        assert.equals(GateInfo.NOT.displayName, GateHelper.get_display_name_from_type(GateInfo.NOT.type))
                    end
                )
                it(
                    "should return Or display name when passed Or type",
                    function()
                        assert.equals(GateInfo.OR.displayName, GateHelper.get_display_name_from_type(GateInfo.OR.type))
                    end
                )
                it(
                    "should return Xnor display name when passed Xnor type",
                    function()
                        assert.equals(GateInfo.XNOR.displayName, GateHelper.get_display_name_from_type(GateInfo.XNOR.type))
                    end
                )
                it(
                    "should return Xor display name when passed Xor type",
                    function()
                        assert.equals(GateInfo.XOR.displayName, GateHelper.get_display_name_from_type(GateInfo.XOR.type))
                    end
                )
                it(
                    "should return nil when passed an incorrect type",
                    function()
                        assert.is_nil(GateHelper.get_display_name_from_type(""))
                    end
                )
            end
        )
        describe(
            "add_output_wire - ",
            function()
                it(
                    "should set output wire to instance of wire passed in",
                    function()
                        local gate = GateHelper.create_gate(GateInfo.AND.type, 2, delay)
                        local outputWire = Wire(1, LogicStates.UNKNOWN)
                        assert.equals(#gate.outputs, 0)
                        GateHelper.add_output_wire(gate, outputWire)
                        assert.same(gate.outputs[1], outputWire)
                    end
                )
                it(
                    "should not leave output wire connected to more than one gate",
                    function()
                        local gate1 = GateHelper.create_gate(GateInfo.AND.type, 2, delay)
                        local gate2 = GateHelper.create_gate(GateInfo.AND.type, 2, delay)
                        local outputWire = Wire(1, LogicStates.UNKNOWN)
                        GateHelper.add_output_wire(gate1, outputWire)
                        assert.same(gate1.outputs[1], outputWire)
                        GateHelper.add_output_wire(gate2, outputWire)
                        assert.same(gate2.outputs[1], outputWire)
                        assert.equals(#gate1.outputs, 0)
                    end
                )
            end
        )
    end
)
