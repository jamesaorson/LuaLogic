local GateInfo = require "constants.gate_info"
local ParseHelper = require "helpers.parse_helper"

describe(
    "Parse helper -",
    function()
        describe(
            "parse_circuit - ",
            function()
                it(
                    "should properly parse a circuit file",
                    function()
                        local circuit = ParseHelper.parse_circuit("circuits/test.circuit")
                        local gates = circuit.gates
                        local wires = circuit.wires
                        
                        assert.equals(#gates[1].inputs, 2)
                        assert.equals(gates[1].delay, 1)
                        assert.equals(gates[1].inputs[1].id, "1")
                        assert.equals(gates[1].inputs[2].id, "2")
                        assert.equals(gates[1].numberOfInputs, 2)
                        assert.equals(gates[1].outputs[1].id, "3")
                        assert.equals(gates[1].type, GateInfo.AND.type)

                        assert.equals(#gates[2].inputs, 3)
                        assert.equals(gates[2].delay, 2)
                        assert.equals(gates[2].inputs[1].id, "3")
                        assert.equals(gates[2].inputs[2].id, "4")
                        assert.equals(gates[2].inputs[3].id, "5")
                        assert.equals(gates[2].numberOfInputs, 3)
                        assert.equals(gates[2].outputs[1].id, "6")
                        assert.equals(gates[2].type, GateInfo.XOR.type)

                        assert.equals(#gates[3].inputs, 1)
                        assert.equals(gates[3].delay, 3)
                        assert.equals(gates[3].inputs[1].id, "6")
                        assert.equals(gates[3].numberOfInputs, 1)
                        assert.equals(gates[3].outputs[1].id, "7")
                        assert.equals(gates[3].type, GateInfo.NOT.type)

                        assert.same(gates[1].outputs[1], gates[2].inputs[1])
                        assert.same(gates[2].outputs[1], gates[3].inputs[1])
                    end
                )
            end
        )
    end
)
