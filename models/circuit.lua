local Circuit = {}

setmetatable(
    Circuit,
    {
        __call = function (self, id, gates, wires, events)
            o = {}
            setmetatable(o, self)
            self.__index = self
            o:initialize(id, gates, wires)
            return o
        end
    }
)

function Circuit:initialize(id, gates, wires, events)
    self.id = id
    if events == nil then
        events = {}
    end
    if gates == nil then
        gates = {}
    end
    if wires == nil then
        wires = {}
    end
    self.events = events
    self.gates = gates
    self.wires = wires
    self.time = 1
end

return Circuit