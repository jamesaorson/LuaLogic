local Circuit = require "models.circuit"
local GateHelper = require "helpers.gate_helper"
local LogicStates = require "constants.logic_states"
local ParseHelper = require "helpers.parse_helper"

local Api = {}

Api.loadedCircuits = {}

function Api.advance_circuit_simulation(circuitId, ticks)
    if ticks == nil or type(ticks) ~= "number" or ticks < 1 then
        ticks = 1
    end

    local circuit = Api.loadedCircuits[circuitId]
    if circuit == nil then
        print("Circuit with id " .. circuitId .. " not loaded")
        return false
    end

    local events = circuit.events
    local gates = circuit.gates
    local wires = circuit.wires
    
    for i = 1, ticks do
        local simulationTime = circuit.time
        if events[simulationTime] ~= nil then
            for _, event in pairs(events[simulationTime]) do             
                local wire = event.wire
                wire.state = event.state
                local gate = wire.outputGate
                
                if gate ~= nil then
                    local timeForNextEvent = simulationTime + gate.delay
                    if events[timeForNextEvent] == nil then
                        events[timeForNextEvent] = {}
                    end
                    local state = gate:simulate_logic()
                    for _, output in pairs(gate.outputs) do
                        table.insert(events[timeForNextEvent], { time = timeForNextEvent, wire = output, state = state} )
                    end
                end
            end
            events[simulationTime] = nil
        end

        local currentState = nil
        for _, wire in pairs(wires) do
            if wire.state == LogicStates.TRUE then
                currentState = "1"
            elseif wire.state == LogicStates.FALSE then
                currentState = "0"
            else
                currentState = "X"
            end
            -- print("Wire id history: ", wire.id)
            -- print("Wire state history: ", wire.state, "\n")
            wire.history = wire.history .. currentState
        end
        circuit.time = circuit.time + 1
    end
end

function Api.begin_simulation(circuitId)
    local circuit = Api.loadedCircuits[circuitId]
    if circuit == nil then
        print("Circuit with id " .. circuitId .. " not loaded")
        return false
    end
    local gates = circuit.gates
    local wires = circuit.wires

    local events = circuit.events
    local simulationTime = 1
    for _, gate in pairs(gates) do
        local newEventTime = simulationTime + gate.delay
        local state = gate:simulate_logic()
        for _, output in pairs(gate.outputs) do
            if events[newEventTime] == nil then
                events[newEventTime] = {}
            end
            table.insert(events[newEventTime], { time = newEventTime, wire = output, state = state })
        end
    end

    return true
end

function Api.load_circuit(circuitToLoad)
    Api.loadedCircuits[circuitToLoad.id] = circuitToLoad
    return circuitToLoad.id
end

function Api.load_circuit_from_file(circuitFile)
    local circuit = ParseHelper.parse_circuit(circuitFile)
    Api.loadedCircuits[circuit.id] = circuit
    return circuit.id
end

function Api.print_gates(circuitId)
    local circuit = Api.loadedCircuits[circuitId]
    if circuit == nil then
        print("Circuit with id " .. circuitId .. " not loaded")
        return false
    end

    for _, gate in pairs(circuit.gates) do
        GateHelper.print_gate(gate)
    end
end

return Api