local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"

local info = GateInfo.NOT

local Not = {}

setmetatable(
    Not,
    {
        __call = function (self, delay)
            o = {}
            setmetatable(o, self)
            self.__index = self
            o:initialize(delay)
            return o
        end
    }
)

function Not:initialize(delay)
    self.delay = delay
    self.inputs = {}
    self.numberOfInputs = 1
    self.outputs = {}
    self.type = info.type
end

function Not:simulate_logic()
    if self.type ~= info.type or #self.inputs ~= 1 then
        return nil
    end

    if self.inputs[1] ~= nil then
        if self.inputs[1].state == LogicStates.TRUE then
            return LogicStates.FALSE
        elseif self.inputs[1].state == LogicStates.FALSE then
            return LogicStates.TRUE
        end
    end
    return LogicStates.UNKNOWN
end

function Not:do_logic()
    local result = self:simulate_logic()
    for _, output  in ipairs(self.outputs) do
        output.state = result
    end
end

return Not