local GateInfo = require "constants.gate_info"
local LogicStates = require "constants.logic_states"

local info = GateInfo.XOR

local Xor = {}

setmetatable(
    Xor,
    {
        __call = function (self, numberOfInputs, delay)
            o = {}
            setmetatable(o, self)
            self.__index = self
            o:initialize(numberOfInputs, delay)
            return o
        end
    }
)

function Xor:initialize(numberOfInputs, delay)
    self.delay = delay
    self.inputs = {}
    self.numberOfInputs = numberOfInputs
    self.outputs = {}
    self.type = info.type
end

function Xor:simulate_logic()
    if self.type ~= info.type or #self.inputs ~= self.numberOfInputs then
        return nil
    end

    local result = LogicStates.FALSE
    for _, wire in ipairs(self.inputs) do
        if wire.state == LogicStates.UNKNOWN then
            return LogicStates.UNKNOWN
        else
            result = result ~ wire.state
        end
    end
    return result
end

function Xor:do_logic()
    local result = self:simulate_logic()
    for _, output  in ipairs(self.outputs) do
        output.state = result
    end
end

return Xor